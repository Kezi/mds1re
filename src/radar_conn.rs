use std::time::Duration;

use log::{debug, info};
use serialport::SerialPort;

pub(crate) struct RadarConn {
    port: Box<dyn SerialPort>,
}

impl RadarConn {
    pub fn new() -> Self {
        let ports = serialport::available_ports().expect("No ports found!");
        for p in &ports {
            debug!("{}", p.port_name);
        }
        let port = serialport::new(&ports.get(0).unwrap().port_name, 115_200)
            .timeout(Duration::from_millis(100))
            .open()
            .expect("Failed to open port");

        RadarConn { port }
    }

    pub fn try_clone(&self) -> Result<Self, serialport::Error> {
        Ok(Self {
            port: self.port.try_clone()?,
        })
    }

    pub fn read(&mut self, buffer: &mut [u8]) -> std::io::Result<usize> {
        self.port.read(buffer)
    }

    pub fn write_command(&mut self, data: &[u8]) -> std::io::Result<()> {
        let ascii = if let Ok(s) = std::str::from_utf8(data) {
            format!("({s})")
        } else {
            Default::default()
        };

        info!("Sending {data:?} {ascii}");

        self.port.write_all(data)
    }
}
