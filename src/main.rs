use clap::Parser;
use commands::{parse_command, RadarCommand};
use log::{info, warn, trace};
use scan::ScanGenerator;
use std::time::Duration;

mod commands;
mod image_handler;
mod radar_conn;
mod scan;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Wait for magnetron warmup
    #[clap(short, long)]
    wait_for_warmup: bool,

    /// Scan interval in milliseconds
    #[clap(long, default_value_t = 200)]
    scan_interval: u64,

    /// Switch on radar
    #[clap(long)]
    on: bool,

    /// Switch off radar
    #[clap(long)]
    off: bool,

    /// Enable graphics (if feature gui is enabled)
    #[clap(long)]
    gui: bool,

    /// Enable tui graphics
    #[clap(long)]
    tui: bool,

    /// Send custom packet, list of u8
    #[clap(short, long, multiple_values = true)]
    exec: Option<Vec<String>>,

    /// Keep program on for input
    #[clap(short, long)]
    follow: bool,
}

fn _main() {
    let env = env_logger::Env::default().filter_or("MY_LOG_LEVEL", "info");
    env_logger::init_from_env(env);

    let cli = Args::parse();

    let mut radar = radar_conn::RadarConn::new();

    let (tx, rx) = std::sync::mpsc::channel();

    let mut port2 = radar.try_clone().unwrap();

    let mut image = image_handler::ImageHandler::new(cli.gui, cli.tui);

    std::thread::spawn(move || {
        let mut buffer = [0u8; 512];

        loop {
            match port2.read(&mut buffer) {
                Ok(bytes) if bytes > 0 => {
                    let data: Vec<u8> = buffer.into_iter().take(bytes).collect();

                    trace!("Received {data:?}");

                    let command = parse_command(&data);

                    match &command {
                        RadarCommand::Unknown(lol) => {
                            image.consume_data(lol);
                        }

                        RadarCommand::MagnetronWarmupDone => {
                            tx.send(()).unwrap();
                        }

                        _ => {
                            info!("Received {command:?}");
                        }
                    }
                }
                Ok(_) => {
                    warn!("Received empty packet?");
                }
                Err(ref e) if e.kind() == std::io::ErrorKind::TimedOut => (),
                Err(e) => warn!("{:?}", e),
            }
        }
    });

    if cli.wait_for_warmup {
        info!("Waiting for magnetron warmup");
        rx.recv().unwrap();
        info!("Warmup done!");
    }

    if cli.on {
        info!("Enabling radar");

        radar.write_command(&[38, 38, 76, 13]).unwrap();
        std::thread::sleep(Duration::from_millis(200));

        radar.write_command(&[38, 38, 77, 13]).unwrap(); // se magnetron non caldo torna [77,0]
        std::thread::sleep(Duration::from_millis(200));

        radar.write_command(&[38, 38, 78, 13]).unwrap();
        std::thread::sleep(Duration::from_millis(200));

        //radar.write_command(&[38, 38, 79, 13]).unwrap();
        //std::thread::sleep(Duration::from_millis(200));
    }

    if cli.off {
        //XXX
        info!("Disabling radar");
        for i in 39..255 {
            for j in 0..255 {
                let data = [38, i, j, 13];

                radar.write_command(&data).unwrap();

                std::thread::sleep(Duration::from_millis(cli.scan_interval));
            }
        }
    }

    /*
        16 da dei risultati
        32 da dei risultati
        36 da meno risultati
        38 avvia
    */

    if let Some(data) = cli.exec {
        let scan = ScanGenerator::new(data.iter().map(|s| s.as_str()).collect());

        for s in scan {
            radar.write_command(&s).unwrap();
            std::thread::sleep(Duration::from_millis(cli.scan_interval));
        }
    }

    std::thread::sleep(Duration::from_millis(cli.scan_interval));

    if cli.follow || cli.tui || cli.gui {
        loop {
            std::thread::sleep(Duration::from_secs(10));
        }
    }
}

#[cfg(feature = "gui")]
#[show_image::main]
fn main() {
    _main()
}

#[cfg(not(feature = "gui"))]
fn main() {
    _main()
}
