use thiserror::Error;

#[derive(Error, Debug)]
pub enum ScanError {
    #[error("invalid pattern")]
    InvalidPattern,

    #[error("invalid range")]
    InvalidRange,

    #[error(transparent)]
    ParseIntError(#[from] std::num::ParseIntError),
}

#[derive(Debug, Clone, PartialEq)]
enum ScanItem {
    Number(u8),
    Range(u8, u8),
}

impl TryFrom<&str> for ScanItem {
    type Error = ScanError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        if let Ok(v) = value.parse::<u8>() {
            return Ok(Self::Number(v));
        }

        let mut spl = value.split("..");

        if let (Some(lower), Some(upper)) = (spl.next(), spl.next()) {
            let lower = lower.parse::<u8>()?;
            let upper = upper.parse::<u8>()?;

            if lower > upper {
                return Err(Self::Error::InvalidRange);
            }

            return Ok(Self::Range(lower, upper));
        }

        Err(Self::Error::InvalidPattern)
    }
}

#[derive(Debug, Clone)]
pub struct ScanGenerator {
    start: Vec<u8>,
    range: Vec<u8>,
    current: u64,
}

impl ScanGenerator {
    pub fn new(scan: Vec<&str>) -> Self {
        let mut start: Vec<u8> = Default::default();
        let mut range: Vec<u8> = Default::default();

        for i in scan {
            let item: ScanItem = i.try_into().unwrap();

            match item {
                ScanItem::Number(v) => {
                    start.push(v);
                    range.push(0);
                }
                ScanItem::Range(l, u) => {
                    start.push(l);
                    range.push((u as u16 - l as u16 + 1).clamp(0, 255) as u8);
                }
            }
        }

        Self {
            current: 0,
            start,
            range,
        }
    }
}

fn map_code(code: &[u8], number: u64) -> Option<Vec<u8>> {
    let mut ret = vec![0; code.len()];

    let remainder =
        code.iter()
            .enumerate()
            .rev()
            .filter(|i| *i.1 != 0)
            .fold(number, |acc, (i, &item)| {
                ret[i] = (acc % item as u64) as u8;
                acc / item as u64
            });

    if remainder != 0 {
        return None;
    }

    Some(ret)
}

impl Iterator for ScanGenerator {
    type Item = Vec<u8>;

    fn next(&mut self) -> Option<Self::Item> {
        let re = map_code(&self.range, self.current)?;

        self.current += 1;

        let sum = re
            .iter()
            .zip(&self.start)
            .map(|f| f.0 + f.1)
            .collect::<Vec<u8>>();

        Some(sum)
    }
}

#[cfg(test)]
mod tests {
    use crate::scan::ScanItem;

    use super::{map_code, ScanGenerator};

    #[test]
    fn scan_item() {
        assert_eq!(ScanItem::Number(12), ScanItem::try_from("12").unwrap());
        assert_eq!(ScanItem::Number(1), ScanItem::try_from("1").unwrap());
        assert_eq!(ScanItem::Number(0), ScanItem::try_from("0").unwrap());
        assert_eq!(ScanItem::Number(255), ScanItem::try_from("255").unwrap());

        ScanItem::try_from("256").unwrap_err();
        ScanItem::try_from("-1").unwrap_err();
        ScanItem::try_from("").unwrap_err();
        ScanItem::try_from("gfd").unwrap_err();

        assert_eq!(
            ScanItem::Range(12, 13),
            ScanItem::try_from("12..13").unwrap()
        );
        assert_eq!(ScanItem::Range(0, 1), ScanItem::try_from("0..1").unwrap());
        assert_eq!(
            ScanItem::Range(255, 255),
            ScanItem::try_from("255..255").unwrap()
        );
        assert_eq!(ScanItem::Range(0, 0), ScanItem::try_from("0..0").unwrap());
        assert_eq!(
            ScanItem::Range(44, 45),
            ScanItem::try_from("44..45").unwrap()
        );

        ScanItem::try_from("0..256").unwrap_err();
        ScanItem::try_from("0..gfdg").unwrap_err();
        ScanItem::try_from("-10..43").unwrap_err();
        ScanItem::try_from("45..23").unwrap_err();
        ScanItem::try_from("45..44").unwrap_err();
    }

    #[test]
    fn scan_generator() {
        let mut s = ScanGenerator::new(vec!["38", "38", "13"]);
        assert_eq!(s.next().unwrap(), [38, 38, 13]);
        assert_eq!(s.next(), None);

        let mut s = ScanGenerator::new(vec!["38", "38", "13..15"]);
        assert_eq!(s.next().unwrap(), [38, 38, 13]);
        assert_eq!(s.next().unwrap(), [38, 38, 14]);
        assert_eq!(s.next().unwrap(), [38, 38, 15]);
        assert_eq!(s.next(), None);

        let mut s = ScanGenerator::new(vec!["38..39", "38", "13..14"]);
        assert_eq!(s.next().unwrap(), [38, 38, 13]);
        assert_eq!(s.next().unwrap(), [38, 38, 14]);
        assert_eq!(s.next().unwrap(), [39, 38, 13]);
        assert_eq!(s.next().unwrap(), [39, 38, 14]);
        assert_eq!(s.next(), None);
    }

    #[test]
    fn test_map_code() {
        let code = [0, 2, 5, 10];

        let re = map_code(&code, 72);
        assert_eq!(re, Some(vec![0, 1, 2, 2]));

        let re = map_code(&code, 2);
        assert_eq!(re, Some(vec![0, 0, 0, 2]));

        let re = map_code(&code, 101);
        assert_eq!(re, None);

        let re = map_code(&code, 0);
        assert_eq!(re, Some(vec![0, 0, 0, 0]));

        let code = [0, 2, 5, 0];

        let re = map_code(&code, 7);
        assert_eq!(re, Some(vec![0, 1, 2, 0]));

        let re = map_code(&code, 9);
        assert_eq!(re, Some(vec![0, 1, 4, 0]));

        //let re = map_code(&[0,0,1], 1);
        //assert_eq!(re, Some(vec![0, 0, 0, 1]));
    }
}
