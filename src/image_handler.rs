use log::info;
#[cfg(feature = "gui")]
use show_image::{ImageInfo, ImageView, WindowProxy};

pub(crate) struct ImageHandler {
    buffer: Vec<u8>,
    in_image: bool,
    #[cfg(feature = "gui")]
    window: Option<WindowProxy>,
    tui: bool,
}

impl ImageHandler {
    pub fn new(_graphics: bool, tui: bool) -> Self {
        #[cfg(feature = "gui")]
        let window = if _graphics {
            let mut opts: show_image::WindowOptions = Default::default();

            opts.default_controls = false;
            opts.resizable = true;
            opts.preserve_aspect_ratio = false;

            let window = show_image::create_window("image", opts).unwrap();

            Some(window)
        } else {
            None
        };

        Self {
            buffer: Vec::new(),
            in_image: false,
            #[cfg(feature = "gui")]
            window,
            tui,
        }
    }

    pub fn consume_data(&mut self, data: &[u8]) -> bool {
        #[cfg(feature = "gui")]
        if self.window.is_none() && self.tui == false {
            return false;
        }

        #[cfg(not(feature = "gui"))]
        if !self.tui {
            return false;
        }

        match data[..] {
            [123, 123, 123, 123, ..] => {
                self.in_image = true;
                self.buffer.clear();
                self.buffer.append(&mut data.to_owned());
            }
            _ => {
                if self.in_image {
                    self.buffer.append(&mut data.to_owned());
                }
                if self.buffer.len() == 14424 {
                    self.in_image = false;

                    self.handle_image();

                    return true;
                }
            }
        }

        self.in_image
    }

    fn handle_image(&self) {
        let _header = &self.buffer[..16];
        let _footer = &self.buffer[self.buffer.len() - 8..];
        let payload = &self.buffer[16..self.buffer.len() - 8];

        info!(
            "Got image l={} (payload = {})",
            self.buffer.len(),
            payload.len()
        );

        #[cfg(feature = "gui")]
        if let Some(window) = &self.window {
            let image = ImageView::new(ImageInfo::mono8(60, 240), &payload);
            window.set_image("image-001", image).unwrap();
        }

        if self.tui {
            ImageHandler::print_tui_image(payload);
        }
    }

    fn print_tui_image(data: &[u8]) {
        let width = 60;
        let height = 240;

        let scale = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. "
            .chars()
            .collect::<Vec<char>>();

        for h in 0..height / 2 {
            for w in 0..width {
                let p = (scale.len() as f64 / 256.0) * ((data[h * width * 2 + w]) as f64);

                print!("{}", scale[p as usize]);
                print!("{}", scale[p as usize]);
                print!("{}", scale[p as usize]);
                print!("{}", scale[p as usize]);
            }
            println!();
        }

        println!("------------");
    }
}
