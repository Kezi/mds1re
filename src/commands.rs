#[derive(Debug)]
pub enum RadarCommand {
    Unknown(Vec<u8>),
    AmpCommand(Vec<u8>),
    HashCommand(Vec<u8>),
    MagnetronWarmupLeft(u8),
    MagnetronWarmupDone,
    PowerOnConfirm,
}

pub fn parse_command(raw_data: &[u8]) -> RadarCommand {
    match raw_data {
        [38, amp_payload @ .., 13] => match amp_payload {
            [97, 17] => RadarCommand::MagnetronWarmupDone,

            [97, left] => RadarCommand::MagnetronWarmupLeft(*left),

            _ => RadarCommand::AmpCommand(amp_payload.to_vec()),
        },

        [35, hash_payload @ .., 13] => match hash_payload {
            [77, 0] => RadarCommand::PowerOnConfirm, // ????
            _ => RadarCommand::HashCommand(hash_payload.to_vec()),
        },

        _ => RadarCommand::Unknown(raw_data.to_vec()),
    }
}
